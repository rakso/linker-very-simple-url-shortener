<?php
require('libraries/userAuthorization.php');
require('libraries/utils.php');

$auth = new UserAuthorization();

cors();
header('Content-Type: application/json');

$entityBody = file_get_contents('php://input');
$request = json_decode($entityBody);


if ($request === null)
    die(json_encode(['error' => 'Bad request. Only JSON in payload is allowed.']));

switch ($request->func) {
    case 'authenticate':
        echo authenticateUser($request->body, $auth);
        break;

    case 'addUser':
        $auth->getCurrentAuthorizedUserIdOrDie();
        echo addUser($request->body);
        break;

    case 'getUsers':
        $auth->getCurrentAuthorizedUserIdOrDie();
        echo getUsers();
        break;

    case 'removeUser':
        $auth->getCurrentAuthorizedUserIdOrDie();
        echo removeUser($request->body);
        break;

    case 'addNewLinkEntry':
        $currentAuthorizedUserId = $auth->getCurrentAuthorizedUserIdOrDie();
        echo addNewLinkEntry($request->body, $currentAuthorizedUserId);
        break;

    case 'getAllLinksEntires':
        $auth->getCurrentAuthorizedUserIdOrDie();
        echo getAllLinksEntires($request->body);
        break;

    case 'editLinkData':
        $currentAuthorizedUserId = $auth->getCurrentAuthorizedUserIdOrDie();
        echo editLinkData($request->body, $currentAuthorizedUserId);
        break;

    case 'removeLinkData':
        $auth->getCurrentAuthorizedUserIdOrDie();
        echo removeLinkData($request->body);
        break;

    default:
        die(json_encode(['error' => 'Bad request. No func & request.']));
}

function authenticateUser($requestBody, $authorizeUserClassInstance)
{
    require('variables.php');
    require('libraries/usersDatabase.php');

    $login = isset($requestBody->login) ? $requestBody->login : '';
    $password = isset($requestBody->password) ? $requestBody->password : '';

    $usersDatabase = new UsersDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);
    $foundUserPk = $usersDatabase->login($login, $password);

    if ($foundUserPk !== false) {
        $authorizeUserClassInstance->setSessionAuthorization($foundUserPk);
        $userFirstAndLastName = $usersDatabase->getUserFirstAndLastName($foundUserPk);

        return json_encode(['success' => true, 'message' => "Hello $userFirstAndLastName[0] $userFirstAndLastName[1]!"]);
    } else {
        die(json_encode(['success' => false, 'message' => 'Can\'t authenticate!']));
    }
}

function addUser($requestBody)
{
    require('variables.php');
    require('libraries/usersDatabase.php');

    $firstName = isset($requestBody->firstName) ? $requestBody->firstName : null;
    $lastName = isset($requestBody->lastName) ? $requestBody->lastName : null;
    $username = isset($requestBody->username) ? $requestBody->username : null;
    $password = isset($requestBody->password) ? $requestBody->password : null;

    if ($firstName  === null || $username === null || $password === null) {
        return json_encode(['success' => false, 'message' => 'Fields "firstName", "username" and "password" are mandatory.']);
    }

    $usersDatabase = new UsersDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);
    try {
        $createdUserId = $usersDatabase->addUser($firstName, $lastName, $username, $password);
    } catch (Exception $error) {
        $mysqlErrorCode = $error->getCode();

        if ($mysqlErrorCode == 1062) {
            return json_encode(['success' => false, 'message' => "Username \"$username\" is already taken."]);
        } else {
            return json_encode(['success' => false, 'message' => 'Can\'t add the user.']);
        }
    }

    return json_encode(['success' => true, 'message' => "Added user. It's ID: \"$createdUserId\"."]);
}

function getUsers()
{
    require('variables.php');
    require('libraries/usersDatabase.php');

    $usersDatabase = new UsersDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);
    $usersFromDatabase = $usersDatabase->getUsers();

    return json_encode(['success' => true, 'result' => $usersFromDatabase]);
}

function removeUser($requestBody)
{
    require('variables.php');
    require('libraries/usersDatabase.php');

    $userId = isset($requestBody->userId) ? $requestBody->userId : '';

    $usersDatabase = new UsersDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);
    $userToDeleteUsername = $usersDatabase->getUsername($userId);

    if ($userToDeleteUsername !== false) {
        try {
            if ($usersDatabase->removeUser($userId)) {
                return json_encode(['success' => true, 'message' => "Deleted user with username \"$userToDeleteUsername\"."]);
            }
        } catch (Exception $error) {
            return json_encode(['success' => false, 'message' => $error->getMessage()]);
        }
    }

    return json_encode(['success' => false, 'message' => 'Can\'t delete the user.']);
}

function addNewLinkEntry($requestBody, $currentAuthorizedUserId)
{
    require('variables.php');
    require('utils.php');
    require('libraries/linksDatabase.php');

    $linkId = isset($requestBody->linkId) && $requestBody->linkId !== '' ? $requestBody->linkId : explode('-', guidv4())[0];
    $linkDescription = isset($requestBody->linkDescription) && $requestBody->linkDescription !== '' ? $requestBody->linkDescription : null;
    $linkValue = isset($requestBody->linkValue) && $requestBody->linkValue !== '' ? $requestBody->linkValue : null;

    if ($linkValue  === null || $linkDescription === null) {
        return json_encode(['success' => false, 'message' => 'Fields "linkValue", "linkDescription" are mandatory.']);
    }

    $linkIdMaxLength = $linkValuesConstraints['idMaxLength'];
    $linkDescriptionMaxLength = $linkValuesConstraints['descriptionMaxLength'];
    $linkValueMaxLength = $linkValuesConstraints['valueMaxLength'];

    $linkIdLength = strlen($linkId);
    $linkDescriptionLength = strlen($linkDescription);
    $linkValueLength = strlen($linkValue);

    if ($linkIdLength > $linkIdMaxLength) {
        return json_encode(['success' => false, 'message' => "Link ID can be a maximum of $linkIdMaxLength characters. (Current length: $linkIdLength)"]);
    }
    if ($linkDescriptionLength > $linkDescriptionMaxLength) {
        return json_encode(['success' => false, 'message' => "Link description can be a maximum of $linkDescriptionMaxLength characters. (Current length: $linkDescriptionLength)"]);
    }
    if ($linkValueLength > $linkValueMaxLength) {
        return json_encode(['success' => false, 'message' => "Link value can be a maximum of $linkValueMaxLength characters. (Current length: $linkValueLength)"]);
    }

    $linksDatabase = new LinksDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);

    $message = null;

    try {
        $linkWithSuchLinkValueInDb = $linksDatabase->getLinkByLiknkValue($linkValue);

        if ($linkWithSuchLinkValueInDb) {
            $linkEntriesWithThisLinkValue = [];
            foreach ($linkWithSuchLinkValueInDb as $key => $value) {
                array_push($linkEntriesWithThisLinkValue, '"' . $value['linkId'] . '"');
            }
            $message = "Warning, there are some link entries with the same link value: " . implode(" ", $linkEntriesWithThisLinkValue);
        }

        $linksDatabase->addLinkData($linkId, $linkDescription, $linkValue, $currentAuthorizedUserId);
    } catch (Exception $error) {
        $mysqlErrorCode = $error->getCode();

        if ($mysqlErrorCode == 1062) {
            $linkWithSuchLinkIdInDb = $linksDatabase->getLinkByLiknkId($linkId);

            return json_encode(['success' => false, 'message' => "Can't add the link entry. The genarted ID \"$linkId\" is already taken by other link (" . $linkWithSuchLinkIdInDb['linkValue'] . "). Try again, or remove the link with this ID."]);
        } else {
            return json_encode(['success' => false, 'message' => 'Can\'t add the link entry. Error code ' . $mysqlErrorCode]);
        }
    }

    return json_encode(['success' => true, 'result' => "Added link entry. It's generated ID is \"$linkId\".", 'message' => $message]);
}

function getAllLinksEntires($requestBody)
{
    require('variables.php');
    require('libraries/linksDatabase.php');

    $linksDatabase = new LinksDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);

    $withRelations = isset($requestBody->withRelations) ? boolval($requestBody->withRelations) : false;

    $allLinks = $linksDatabase->getAllLinks($withRelations);

    return json_encode(['success' => true, 'result' => $allLinks]);
}

function editLinkData($requestBody, $currentAuthorizedUserId)
{
    require('variables.php');
    require('libraries/linksDatabase.php');

    $linksDatabase = new LinksDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);

    $linkId = isset($requestBody->linkId) && $requestBody->linkId !== '' ? $requestBody->linkId : null;
    $linkDescription = isset($requestBody->linkDescription) && $requestBody->linkDescription !== '' ? $requestBody->linkDescription : null;
    $linkValue = isset($requestBody->linkValue) && $requestBody->linkValue !== '' ? $requestBody->linkValue : null;

    $linkData = $linksDatabase->getLinkByLiknkId($linkId);
    if (!$linkData) {
        return json_encode(['success' => false, 'message' => "Given link ID $linkId does not exist or its malformed."]);
    }

    if ($linksDatabase->updateLinkData($linkId, $linkDescription, $linkValue, $currentAuthorizedUserId)) {
        return json_encode(['success' => true, 'message' => "Updated link data with link ID \"$linkId\"."]);
    }

    return json_encode(['success' => false, 'message' => 'Can\'t update the link data. Or no changes have been made.']);
}

function removeLinkData($requestBody)
{
    require('variables.php');
    require('libraries/linksDatabase.php');

    $linksDatabase = new LinksDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);

    $linkId = isset($requestBody->linkId) && $requestBody->linkId !== '' ? $requestBody->linkId : null;

    $linkData = $linksDatabase->getLinkByLiknkId($linkId);
    if (!$linkData) {
        return json_encode(['success' => false, 'message' => "Given link ID $linkId does not exist or its malformed."]);
    }

    if ($linksDatabase->removeLinkData($linkId)) {
        return json_encode(['success' => true, 'result' => "Deleted link with link ID \"$linkId\"."]);
    }

    return json_encode(['success' => false, 'message' => 'Can\'t delete the link data.']);
}

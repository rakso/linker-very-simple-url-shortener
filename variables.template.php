<?php
$db = [
    'servername' => 'localhost',
    'username' => '______',
    'password' => '______',
    'name' => '______',
    'useLegacyMethods' => false,
];

$linkValuesConstraints = [
    'idMaxLength' => 8,
    'descriptionMaxLength' => 200,
    'valueMaxLength' => 8192,
];

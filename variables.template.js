const linksEndpoint = "../api.php";

const userInterfaceUrl = "https://website/";
const urlRewriteEnabled = false; // Set to true is you are using URL rewrite.

const timeZone = null; // Set to null for autodetect. List of timezones: https://gist.github.com/diogocapela/12c6617fc87607d11fd62d2a4f42b02a#file-moment-js-timezones-txt
const localeForDateTimeString = null; // Set to null for autodetect. List of locale codes: https://github.com/TiagoDanin/Locale-Codes#locale-list

<?php
require('../libraries/usersDatabase.php');
require('../utils.php');

class CreateDb
{
    private $connection;
    private $sql = NULL;

    private $servername;
    private $username;
    private $password;
    private $dbNameToUseOrCreate;
    private $linkValuesConstraints;
    private $useLegacyMethod;

    function __construct($servername, $username, $password, $dbname, $useLegacyMethod, $linkValuesConstraints)
    {
        $this->servername = $servername;
        $this->username = $username;
        $this->password = $password;
        $this->dbNameToUseOrCreate = $dbname;
        $this->useLegacyMethod = $useLegacyMethod;
        $this->linkValuesConstraints = $linkValuesConstraints;

        $this->connection = new mysqli($servername, $username, $password);

        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        };

        // Set charset
        $this->connection->set_charset("utf8mb4");
    }

    private function doRawQuery()
    {
        return $this->connection->query($this->sql);
    }

    private function checkIfTableExists($tableName)
    {
        $this->sql = "USE `$this->dbNameToUseOrCreate`;";
        try {
            $result = $this->doRawQuery(true);
        } catch (\Throwable $th) {
            if ($th->getCode() === 1049) { // Not known DB
                return 0;
            }
            throw $th;
        }

        $this->sql = "SELECT `TABLE_NAME` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$tableName';";
        $result = $this->doRawQuery();
        return $result->num_rows;
    }

    private function checkIfLinksDataExists()
    {
        return $this->checkIfTableExists('linksData');
    }

    private function createDefaultUser()
    {
        $firstName = 'Admin';
        $lastName = NULL;
        $username = 'admin';
        $password = explode('-', guidv4())[1];

        $usersDatabase = new UsersDatabase($this->servername, $this->username, $this->password, $this->dbNameToUseOrCreate, $this->useLegacyMethod);
        $result = $usersDatabase->addUser($firstName, $lastName, $username, $password);

        if ($result) {
            return "<br>Created admin user. Username: <b>$username</b> Generated password: <b>$password</b> - please remember this password and change it as soon as possible.";
        }
        throw new Exception("Error creating new user");
    }

    public function doCreate()
    {
        $linkIdMaxLength = $this->linkValuesConstraints['idMaxLength'];
        $linkDescriptionMaxLength = $this->linkValuesConstraints['descriptionMaxLength'];
        $linkValueMaxLength = $this->linkValuesConstraints['valueMaxLength'];

        $sqlQueries = [
            'createDb' => "CREATE DATABASE IF NOT EXISTS `$this->dbNameToUseOrCreate` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci;",

            'useDb' => "USE `$this->dbNameToUseOrCreate`;",

            'createTableLinkAccess' => 'CREATE TABLE `linkAccess` (
            `linkPk` bigint UNSIGNED NOT NULL,
            `accessCount` int NOT NULL,
            `lastAccess` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `firstAccess` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;',

            'createTableLinksData' => ' CREATE TABLE `linksData` (
            `pk` bigint UNSIGNED NOT NULL,
            `linkId` char(' . $linkIdMaxLength . ') COLLATE utf8mb4_polish_ci NOT NULL,
            `description` varchar(' . $linkDescriptionMaxLength . ') COLLATE utf8mb4_polish_ci NOT NULL,
            `linkValue` varchar(' . $linkValueMaxLength . ') CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
            `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `createdBy` bigint UNSIGNED NOT NULL,
            `updatedAt` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
            `updatedBy` bigint UNSIGNED DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;',

            'createTableUsers' => "CREATE TABLE `users` (
            `pk` bigint UNSIGNED NOT NULL,
            `firstName` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL,
            `lastName` varchar(20) COLLATE utf8mb4_polish_ci DEFAULT NULL,
            `username` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL,
            `password` varchar(72) COLLATE utf8mb4_polish_ci NOT NULL,
            `isActive` tinyint(1) NOT NULL DEFAULT '1'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci",

            'createIndexLinkAccess' => 'ALTER TABLE `linkAccess`
            ADD UNIQUE KEY `linkPk` (`linkPk`);',

            'createIndexLinksData' => 'ALTER TABLE `linksData`
            ADD UNIQUE KEY `pk` (`pk`),
            ADD UNIQUE KEY `linkId` (`linkId`),
            ADD KEY `linksData_ibfk_1` (`createdBy`),
            ADD KEY `updatedBy` (`updatedBy`);',

            'createIndexUsers' => 'ALTER TABLE `users`
            ADD UNIQUE KEY `pk` (`pk`),
            ADD UNIQUE KEY `username` (`username`);',

            'createAutoIncrementLinksData' =>  'ALTER TABLE `linksData`
            MODIFY `pk` bigint UNSIGNED NOT NULL AUTO_INCREMENT;',

            'createAutoIncrementUsers' =>  'ALTER TABLE `users`
            MODIFY `pk` bigint UNSIGNED NOT NULL AUTO_INCREMENT;',

            'createConstraintLinkAccess' =>  'ALTER TABLE `linkAccess`
            ADD CONSTRAINT `linkAccess_ibfk_1` FOREIGN KEY (`linkPk`) REFERENCES `linksData` (`pk`) ON DELETE CASCADE ON UPDATE CASCADE;',

            'createConstraintLinksData' =>  'ALTER TABLE `linksData`
            ADD CONSTRAINT `linksData_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `users` (`pk`) ON DELETE RESTRICT ON UPDATE CASCADE,
            ADD CONSTRAINT `linksData_ibfk_2` FOREIGN KEY (`updatedBy`) REFERENCES `users` (`pk`) ON DELETE RESTRICT ON UPDATE CASCADE;',
        ];

        $anyErros = false;

        if ($this->checkIfLinksDataExists()) {
            exit("Warning! Table seems to be already created. If you want to recreate the database, please drop the current database. Installation cancelled.");
        } else {
            foreach ($sqlQueries as $queryName => $queryValue) {
                $this->sql = $queryValue;

                try {
                    if ($this->doRawQuery()) {
                        echo "Query \"$queryName\": <b>OK!</b><br>";
                    } else {
                        $anyErros = true;
                        echo "Query: \"$queryName\": <b>Failed! (" . $this->connection->error . ")</b><br>";
                    }
                } catch (\Throwable $th) {
                    $anyErros = true;
                    echo "Query \"$queryName\": <b>Failed with exception:</b> " . $th->getMessage() . '<br>';
                }
            }
        }

        if (!$anyErros) {

            try {
                echo $this->createDefaultUser();
                echo '<br>Now you can navigate to the <a href="./">admin panel</a>, login (using the credentials above) and start creating links!';
            } catch (\Throwable $th) {
                echo 'Failed to create user! Please drop the database and restart the installation process. Error message: ' . $th->getMessage();
            }
        }
    }
}

require('../variables.php');

$createDb = new CreateDb($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods'], $linkValuesConstraints);
$creationResult = $createDb->doCreate();

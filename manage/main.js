class LinkerAPI {
  constructor(apiUrl) {
    this._apiUrl = apiUrl;
  }

  async _sendRequest(func, body) {
    const response = await fetch(`${this._apiUrl}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        func,
        body,
      }),
    });

    return response.json().then((response) => {
      if (!response.success) {
        throw new Error(response.message);
      }
      return response;
    });
  }

  async addNewLinkEntry(linkId, linkDescription, linkValue) {
    return this._sendRequest("addNewLinkEntry", {
      linkId,
      linkDescription,
      linkValue,
    });
  }

  async getAllLinksData(withRelations = false) {
    return this._sendRequest("getAllLinksEntires", { withRelations });
  }

  async removeLinkData(linkId) {
    return this._sendRequest("removeLinkData", { linkId });
  }
}

class RowGenerator {
  constructor(tableElement, userInterfaceUrl, urlRewriteEnabled = false) {
    this.tableBody = tableElement.tBodies[0];

    this.tableRowData = {
      button: null,
      linkId: null,
      linkDescription: null,
      linkValue: null,
      accessCount: null,
      lastAccess: null,
      createdBy: null,
    };
    this.tableRow = null;

    this.callbackEditRow = { func: null, context: null };
    this.callbackDeleteRow = { func: null, context: null };

    this.userInterfaceUrl = userInterfaceUrl;
    this.urlRewriteEnabled = urlRewriteEnabled;
  }

  setEditRowCallback(func, context = this) {
    this.callbackEditRow.func = func;
    this.callbackEditRow.context = context;
  }

  setDeleteRowCallback(func, context = this) {
    this.callbackDeleteRow.func = func;
    this.callbackDeleteRow.context = context;
  }

  _preapreRow() {
    this.tableRowData.button = document.createElement("td");
    this.tableRowData.linkId = document.createElement("th");
    this.tableRowData.linkId.setAttribute("scope", "row");
    this.tableRowData.linkDescription = document.createElement("td");
    this.tableRowData.linkValue = document.createElement("td");
    this.tableRowData.accessCount = document.createElement("td");
    this.tableRowData.lastAccess = document.createElement("td");
    this.tableRowData.createdBy = document.createElement("td");

    const tableRow = document.createElement("tr");
    tableRow.appendChild(this.tableRowData.button);
    tableRow.appendChild(this.tableRowData.linkId);
    tableRow.appendChild(this.tableRowData.linkDescription);
    tableRow.appendChild(this.tableRowData.linkValue);
    tableRow.appendChild(this.tableRowData.accessCount);
    tableRow.appendChild(this.tableRowData.lastAccess);
    tableRow.appendChild(this.tableRowData.createdBy);

    return tableRow;
  }

  addRow(
    linkId,
    linkDescription,
    linkValue,
    accessCount,
    firstAccess,
    lastAccess,
    createdBy,
    createdAt,
    updatedBy,
    updatedAt
  ) {
    const row = this._preapreRow();

    const dropdownButton = document.createElement("button");
    dropdownButton.classList.add(
      "btn",
      "btn-secondary",
      "btn-sm",
      "dropdown-toggle"
    );
    dropdownButton.setAttribute("data-bs-toggle", "dropdown");
    dropdownButton.setAttribute("aria-expanded", "false");
    dropdownButton.innerText = "Menu";

    const dropdownList = document.createElement("ul");
    dropdownList.classList.add("dropdown-menu");

    const listEntryEdit = document.createElement("li");
    const listDropdownItemEdit = document.createElement("button");
    listDropdownItemEdit.classList.add("dropdown-item");
    listDropdownItemEdit.innerText = "Edit this entry";
    listDropdownItemEdit.addEventListener("click", () => {
      this.callbackEditRow.func.call(this.callbackEditRow.context, row);
    });
    listEntryEdit.appendChild(listDropdownItemEdit);
    dropdownList.appendChild(listEntryEdit);

    const listEntryDivider = document.createElement("li");
    listEntryDivider.innerHTML = '<hr class="dropdown-divider">';
    dropdownList.appendChild(listEntryDivider);

    const listEntryDelete = document.createElement("li");
    const listDropdownItemDelete = document.createElement("button");
    listDropdownItemDelete.classList.add("dropdown-item");
    listDropdownItemDelete.innerText = "Delete this redirect";
    listDropdownItemDelete.addEventListener("click", () => {
      this.callbackDeleteRow.func.call(
        this.callbackDeleteRow.context,
        linkId,
        row
      );
    });
    listEntryDelete.appendChild(listDropdownItemDelete);
    dropdownList.appendChild(listEntryDelete);

    this.tableRowData.button.appendChild(dropdownButton);
    this.tableRowData.button.appendChild(dropdownList);

    const redirectionLink = document.createElement("a");
    redirectionLink.href = this.urlRewriteEnabled
      ? `${this.userInterfaceUrl}${linkId}`
      : `${this.userInterfaceUrl}?i=${linkId}`;
    redirectionLink.innerText = linkId;
    redirectionLink.target = "_blank";
    this.tableRowData.linkId.appendChild(redirectionLink);

    this.tableRowData.linkDescription.innerText = linkDescription;

    const linkValueLink = document.createElement("a");
    linkValueLink.href = linkValue;
    linkValueLink.innerText =
      linkValue.length < 50 ? linkValue : `${linkValue.slice(0, 50)}...`;
    linkValueLink.target = "_blank";
    this.tableRowData.linkValue.appendChild(linkValueLink);

    this.tableRowData.accessCount.innerText = accessCount;

    this.tableRowData.lastAccess.setAttribute("data-bs-toggle", "tooltip");
    this.tableRowData.lastAccess.setAttribute("data-bs-placement", "bottom");
    this.tableRowData.lastAccess.setAttribute("data-bs-html", "true");
    this.tableRowData.lastAccess.setAttribute(
      "title",
      `First access: ${firstAccess}`
    );
    new bootstrap.Tooltip(this.tableRowData.lastAccess);
    this.tableRowData.lastAccess.innerText = lastAccess;

    this.tableRowData.createdBy.setAttribute("data-bs-toggle", "tooltip");
    this.tableRowData.createdBy.setAttribute("data-bs-placement", "bottom");
    this.tableRowData.createdBy.setAttribute("data-bs-html", "true");
    const updateInfo = updatedBy
      ? `Updated by: ${updatedBy}<br>
    Updated at: ${updatedAt}<br>`
      : "Never updated.";
    this.tableRowData.createdBy.setAttribute(
      "title",
      `Created at: ${createdAt}<br>
      ${updateInfo}`
    );
    new bootstrap.Tooltip(this.tableRowData.createdBy);
    this.tableRowData.createdBy.innerText = createdBy;

    this.tableBody.appendChild(row);
  }
}

class NewData {
  constructor(idInput, descriptionInput, linkValueInput, addNewLinkButton) {
    this.id = idInput;
    this.description = descriptionInput;
    this.linkValue = linkValueInput;
    this.addButton = addNewLinkButton;

    this.callback = { func: null, context: null };
  }

  setCallback(func, context = this) {
    this.callback.func = func;
    this.callback.context = context;
  }

  checkEnter({ code }) {
    if (code === "Enter") {
      this.callback.func.call(
        this.callback.context,
        this.id.value,
        this.description.value,
        this.linkValue.value
      );
    }
  }

  clear() {
    this.id.value = null;
    this.description.value = null;
    this.linkValue.value = null;
  }

  listen() {
    this.id.addEventListener("keydown", (event) => {
      this.checkEnter(event);
    });
    this.description.addEventListener("keydown", (event) => {
      this.checkEnter(event);
    });
    this.linkValue.addEventListener("keydown", (event) => {
      this.checkEnter(event);
    });
    this.addButton.addEventListener("click", () => {
      this.checkEnter({ code: "Enter" });
    });
  }
}

// https://stackoverflow.com/a/54127122/
function convertTZ(date, tzString) {
  return new Date(
    (typeof date === "string" ? new Date(`${date} UTC`) : date).toLocaleString(
      "en-US",
      {
        timeZone: tzString,
      }
    )
  );
}

function prepareDateTimeString(dateObject) {
  const clientTimeZone =
    timeZone !== null
      ? timeZone
      : Intl.DateTimeFormat().resolvedOptions().timeZone;
  const clientLocale =
    localeForDateTimeString !== null
      ? localeForDateTimeString
      : window.navigator.userLanguage || window.navigator.language;

  const dateTimeStringOptions = {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
  };
  return convertTZ(dateObject, clientTimeZone).toLocaleDateString(
    clientLocale,
    dateTimeStringOptions
  );
}

document.addEventListener("DOMContentLoaded", async () => {
  const linkIdToSet = document.getElementById("linkIdToSet");
  const linkDescriptionToSet = document.getElementById("linkDescriptionToSet");
  const linkValueToSet = document.getElementById("linkValueToSet");
  const buttonAddNewLink = document.getElementById("buttonAddNewLink");

  const api = new LinkerAPI(linksEndpoint);

  const newData = new NewData(
    linkIdToSet,
    linkDescriptionToSet,
    linkValueToSet,
    buttonAddNewLink
  );

  const tableRow = new RowGenerator(
    document.getElementById("linksTable"),
    userInterfaceUrl,
    urlRewriteEnabled
  );

  const allLinksData = await api.getAllLinksData(1);

  newData.listen();

  newData.setCallback((linkId, linkDescription, linkValue) => {
    api
      .addNewLinkEntry(linkId, linkDescription, linkValue)
      .then(({ message }) => {
        newData.clear();

        if (message) {
          alert(message);
        }

        location.reload();
      })
      .catch((error) => alert(error.message));
  });

  allLinksData.result.forEach(
    ({ linkId, description, linkValue, created, updated, access }) => {
      let accessCount = 0;
      let firstAccess = "Not yet viewed";
      let lastAccess = "-";

      if (access != null) {
        accessCount = access.count;
        firstAccess = prepareDateTimeString(access.date.first);
        lastAccess = prepareDateTimeString(access.date.last);
      }

      const createdUserNameAndSurname = `${
        created.by.name ? created.by.name : ""
      } ${created.by.surname ? created.by.surname : ""}`.trim();

      let updatedUserNameAndSurname = undefined;
      let updatedAt = undefined;

      const createdAt = prepareDateTimeString(created.at);

      if (updated !== null) {
        updatedUserNameAndSurname = `${
          updated.by.name ? updated.by.name : ""
        } ${updated.by.surname ? updated.by.surname : ""}`.trim();
        updatedAt = prepareDateTimeString(updated.at);
      }

      tableRow.addRow(
        linkId,
        description,
        linkValue,
        accessCount,
        firstAccess,
        lastAccess,
        createdUserNameAndSurname,
        createdAt,
        updatedUserNameAndSurname,
        updatedAt
      );
    }
  );

  tableRow.setEditRowCallback((row) => {
    console.log("Edit: ", row);
    alert(
      "Sorry, currently not implemented. Try to delete this entry, and then create new entry (access data will be lost)."
    );
  });
  tableRow.setDeleteRowCallback((linkId, row) => {
    const question = confirm(
      `Do you really want to remove the redirect with the ID ${linkId}?`
    );

    if (question) {
      api
        .removeLinkData(linkId)
        .then(({ result }) => {
          alert(result);
          location.reload();
        })
        .catch((error) => alert(error.message));
    }
  });
});

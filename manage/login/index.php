<?php
require('../../libraries/userAuthorization.php');
require('../../libraries/utils.php');

$auth = new UserAuthorization();

try {
    $auth->getCurrentAuthorizedUserId();
    echo "Warning! Already logged in - logging out...";
    $auth->unsetSessionAuthorization();
} catch (\Throwable $th) {
}

function authenticateUser($login, $password, $authorizeUserClassInstance)
{
    require('../../variables.php');
    require('../../libraries/usersDatabase.php');

    $usersDatabase = new UsersDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);
    $foundUserPk = $usersDatabase->login($login, $password);

    if ($foundUserPk !== false) {
        $authorizeUserClassInstance->setSessionAuthorization($foundUserPk);
        $userFirstAndLastName = $usersDatabase->getUserFirstAndLastName($foundUserPk);

        return $userFirstAndLastName;
    }
    return false;
}

$login = isset($_POST['login']) && $_POST['login'] !== '' ? $_POST['login'] : null;
$password = isset($_POST['password']) && $_POST['password'] !== '' ? $_POST['password'] : null;

if ($login !== null && $password !== null) {
    $userFirstAndLastName = authenticateUser($login, $password, $auth);

    if ($userFirstAndLastName !== false) {
        $where = isset($_GET['r']) && $_GET['r'] !== '' ? $_GET['r'] : '../';
        header("Location: $where");
        exit();
    } else {
        $messageForUser = 'Can\'t log in. Login or password may be incorrect.';
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Linker - log in</title>
    <link rel="icon" type="image/png" href="../../icon.png">
</head>

<body style="background-color: #757575;">
    <section class="vh-100 gradient-custom">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card bg-dark text-white" style="border-radius: 1rem;">
                        <div class="card-body p-5 text-center">

                            <div class="mb-md-5 mt-md-4 pb-5">
                                <form ction="" method="post">
                                    <h2 class="fw-bold mb-2 text-uppercase">Login</h2>
                                    <p class="text-white-50 mb-5">Please enter your login and password!</p>
                                    <?php echo isset($messageForUser) ? "<p style=\"color: red;\">$messageForUser</p>" : ''; ?>

                                    <div class="form-outline form-white mb-4">
                                        <input type="text" id="typeEmailX" name="login" class="form-control form-control-lg" <?php echo isset($messageForUser) ? "value=\"$login\"" : ''; ?> />
                                        <label class="form-label" for="typeEmailX">Login</label>
                                    </div>

                                    <div class="form-outline form-white mb-4">
                                        <input type="password" id="typePasswordX" name="password" class="form-control form-control-lg" />
                                        <label class="form-label" for="typePasswordX">Password</label>
                                    </div>

                                    <button class="btn btn-outline-light btn-lg px-5" type="submit">Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</body>

</html>
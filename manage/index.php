<?php
require('../libraries/userAuthorization.php');
require('../libraries/utils.php');

$auth = new UserAuthorization();

try {
    $auth->getCurrentAuthorizedUserId();
} catch (\Throwable $th) {
    $actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    header("Location: ./login/?r=$actualLink");
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <script src="../variables.js"></script>
    <script src="./main.js"></script>

    <title>Linker - manage</title>
    <link rel="icon" type="image/png" href="../icon.png">
</head>

<body>
    <div class="container-xl">
        <div class="row">
            <table class="table table-striped table-hover" id="linksTable">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Link ID</th>
                        <th scope="col">Link description</th>
                        <th scope="col">Redirect URL</th>
                        <th scope="col">Redirect count</th>
                        <th scope="col">Last redirection date</th>
                        <th scope="col">Added by</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <th scope="col">
                            <input id="linkIdToSet" name="linkIdToSet" placeholder="link ID (optional)" type="text" class="form-control" title="">
                        </th>
                        <td><input id="linkDescriptionToSet" name="linkDescriptionToSet" placeholder="Description (mandatory)" type="text" required="required" class="form-control"></td>
                        <td><input id="linkValueToSet" name="linkValueToSet" placeholder="Redirection URL (mandatory)" type="text" required="required" class="form-control"></td>
                        <td colspan="3"><button id="buttonAddNewLink" name="buttonAddNewLink" type="button" class="btn btn-outline-success">Add this entry</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>
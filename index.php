<?php
require('variables.php');
require('libraries/linksDatabase.php');


function getLinkUrlAndBumpAccessCounter($linkId, $db)
{
    $linksDatabase = new LinksDatabase($db['servername'], $db['username'], $db['password'], $db['name'], $db['useLegacyMethods']);

    $linkData = $linksDatabase->getLinkByLiknkId($linkId);
    if (!$linkData) {
        throw new Exception("The link ID \"$linkId\" does not exist or is incorrect.", 1);
    }

    if (!$linksDatabase->addLinkAccess($linkId)) {
        throw new Exception("Failed to add metadata.", 1);
    }

    return $linkData['linkValue'];
}

$linkIdKeyName = 'i';
$linkIdFromQuery = isset($_GET[$linkIdKeyName]) && $_GET[$linkIdKeyName] !== '' ? $_GET[$linkIdKeyName] : null;


if ($linkIdFromQuery !== null) {
    $redirectionLink = '';
    try {
        $redirectionLink = getLinkUrlAndBumpAccessCounter($linkIdFromQuery, $db);

        header("Location: $redirectionLink");
        exit;
    } catch (\Throwable $th) {
        $infoForUser = $th->getMessage();
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Linker - enter link ID</title>
    <link rel="icon" type="image/png" href="icon.png">
</head>

<body style="background-color: #757575;">
    <div class="container">
        <div class="row justify-content-md-center" style="margin-top: 10em;">
            <div class="col-sm-4">
                <form>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-angle-right"></i>
                            </div>
                        </div>
                        <input id="linkId" name="i" placeholder="Enter your <?php echo $linkValuesConstraints['idMaxLength']; ?> character link ID here" type="text" required="required" class="form-control">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <i class="fa fa-angle-left"></i>
                            </div>
                        </div>
                    </div>
                    <?php echo isset($infoForUser) ? '<span id="linkIdHelpBlock" class="form-text" style="color: white;" autocomplete="false">' . strip_tags($infoForUser) . '</span>' : ''; ?>
            </div>
            </form>
        </div>
    </div>
    </div>
    <script>
        const inputLink = document.getElementById('linkId');
        const linkIdHelpBlock = document.getElementById('linkIdHelpBlock');
        inputLink.focus();
        const form = document.getElementsByTagName('form')[0];
        inputLink.addEventListener("input", () => {
            const linkIdValue = inputLink.value;
            const linkIdLength = linkIdValue.length;
            if (linkIdValue.includes(' ')) {
                inputLink.value = linkIdValue.replace(' ', '');
            }

            if (linkIdLength >= <?php echo $linkValuesConstraints['idMaxLength']; ?>) {
                form.submit();
                inputLink.disabled = true;
                linkIdHelpBlock.innerText = 'Processing...';
            }
        });
    </script>
</body>

</html>
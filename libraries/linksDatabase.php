<?php
require(__DIR__ .  '/../libraries/simpleDbClient.php');

class LinksDatabase extends SimpleDbClient
{
    function addLinkData($linkId, $description, $linkValue, $addedById)
    {
        $this->sql = "INSERT INTO `linksData` (`pk`, `linkId`, `description`, `linkValue`, `createdAt`, `createdBy`) VALUES (NULL, ?, ?, ?, UTC_TIMESTAMP, ?);";
        $this->sqlParams = [$linkId, $description, $linkValue, $addedById];

        return $this->insert();
    }

    private function getLinkBy($whereQuery, $getWithRealtions)
    {
        if ($getWithRealtions) {
            $this->sql = "SELECT 
            `linksData`.`pk`, 
            `linksData`.`linkId`, 
            `linksData`.`description`, 
            `linksData`.`linkValue`, 
            `linksData`.`createdAt`, 
            `createdBy`.`firstName` `createdByFirstName`, 
            `createdBy`.`lastName` `createdByLastName`, 
            `createdBy`.`username` `createdByUsername`, 
            `updatedBy`.`firstName` `updatedByFirstName`, 
            `updatedBy`.`lastName` `updatedByLastName`, 
            `updatedBy`.`username` `updatedByUsername`,
            `linksData`.`updatedAt`,
            `linkAccess`.`accessCount`,
            `linkAccess`.`lastAccess`,
            `linkAccess`.`firstAccess` FROM `linksData` 
            LEFT JOIN `linkAccess` ON `linksData`.`pk` = `linkAccess`.`linkPk` 
            LEFT JOIN `users` as `createdBy` ON `linksData`.`createdBy`=`createdBy`.`pk` 
            LEFT JOIN `users` as `updatedBy` ON `linksData`.`updatedBy`=`updatedBy`.`pk` 
            WHERE $whereQuery ORDER BY `linksData`.`createdAt` DESC;";
        } else {
            $this->sql = "SELECT `pk`, `linkId`, `description`, `linkValue`, `createdAt`, `createdBy` FROM `linksData` WHERE $whereQuery;";
        }

        $result = $this->doQuery(true);

        $data = [];

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                if (isset($row['createdByUsername'])) {
                    $createdByData =  [
                        'name' => $row['createdByFirstName'],
                        'surname' => $row['createdByLastName'],
                        'username' => $row['createdByUsername'],
                    ];
                } else {
                    $createdByData =  $row['createdBy'];
                }

                $createdData = [
                    'at' => $row['createdAt'],
                    'by' => $createdByData,
                ];

                if (isset($row['updatedAt'])) {
                    $updatedData =  [
                        'at' => $row['updatedAt'],
                        'by' => [
                            'name' => $row['updatedByFirstName'],
                            'surname' => $row['updatedByLastName'],
                            'username' => $row['updatedByUsername'],
                        ]
                    ];
                } else {
                    $updatedData =  null;
                }

                if (isset($row['accessCount'])) {
                    $accessdData =  [
                        'count' => $row['accessCount'],
                        'date' => [
                            'first' => $row['firstAccess'],
                            'last' => $row['lastAccess'],
                        ]
                    ];
                } else {
                    $accessdData = null;
                }

                $dataToPush = [
                    'linkDbId' => $row['pk'],
                    'linkId' => $row['linkId'],
                    'description' => $row['description'],
                    'linkValue' => $row['linkValue'],
                    'created' => $createdData,
                    'updated' => $updatedData,
                    'access' => $accessdData,

                ];

                array_push(
                    $data,
                    $dataToPush
                );
            }
        }

        return $data;
    }

    function getLinkByLiknkValue($linkValue, $getWithRealtions = false)
    {
        $this->sqlParams = [$linkValue];
        return $this->getLinkBy("`linkValue` = ?", $getWithRealtions);
    }

    function getLinkByLiknkId($linkId, $getWithRealtions = false)
    {
        $this->sqlParams = [$linkId];
        $foundLinkData = $this->getLinkBy("`linkId` = ?", $getWithRealtions);
        return $foundLinkData === [] ? $foundLinkData : $foundLinkData[0];
    }

    function getAllLinks($getWithRealtions = false)
    {
        return $this->getLinkBy("true", $getWithRealtions);
    }


    function createEntryInLinkAccess($linkId, $initialAccessCountValue = 1)
    {
        $this->sql = "INSERT INTO `linkAccess` (`linkPk`, `accessCount`, `lastAccess`, `firstAccess`) VALUES ((SELECT `linksData`.`pk` FROM `linksData` WHERE `linksData`.`linkId`=?), ?, UTC_TIMESTAMP, UTC_TIMESTAMP);";
        $this->sqlParams = [$linkId, $initialAccessCountValue];

        return $this->insert();
    }

    function updateLinkAccessEntry($linkId, $accesCount)
    {
        $this->sql = "SET @@session.time_zone='+00:00';";
        $this->sqlParams = [];
        $this->doQuery();

        $this->sql = "UPDATE `linkAccess` SET `accessCount` = ? WHERE `linkAccess`.`linkPk`= (SELECT `linksData`.`pk` FROM `linksData` WHERE `linksData`.`linkId` = ?);";
        $this->sqlParams = [$accesCount, $linkId];

        return $this->doRowModification();
    }

    function updateLinkData($linkIdToUpdate, $description, $linkValue, $updatedBy)
    {
        $partOfQuery = '';
        $partOfQueryParams = [];

        if ($description !== null) {
            $partOfQuery .= "`description` = ?, ";
            array_push($partOfQueryParams, $description);
        }
        if ($linkValue !== null) {
            $partOfQuery .= "`linkValue` = ?, ";
            array_push($partOfQueryParams, $linkValue);
        }

        $this->sql = "UPDATE `linksData` SET $partOfQuery `updatedBy` = ? WHERE `linksData`.`linkId` = ?;";
        $this->sqlParams = array_merge($partOfQueryParams, [$updatedBy, $linkIdToUpdate]);

        return $this->doRowModification();
    }

    function getLinkAccessCountByLinkId($linkId)
    {
        $this->sql = "SELECT `linkAccess`.`accessCount` FROM `linksData` LEFT JOIN `linkAccess` ON `linkAccess`.`linkPk` = `linksData`.`pk` WHERE `linksData`.`linkId` = ?;";
        $this->sqlParams = [$linkId];


        $result = $this->doQuery(true)->fetch_assoc();

        if ($result === null) {
            return false;
        }
        return $result['accessCount'];
    }

    function addLinkAccess($linkId)
    {
        $linkCurrentAccessCount = $this->getLinkAccessCountByLinkId($linkId);

        if (!$linkCurrentAccessCount) {
            try {
                $this->createEntryInLinkAccess($linkId);
                return true;
            } catch (Exception $error) {
                return false;
            }
        }

        return $this->updateLinkAccessEntry($linkId, $linkCurrentAccessCount += 1) >= 1;
    }

    function removeLinkData($linkId)
    {
        $this->sql = "DELETE FROM `linksData` WHERE `linksData`.`linkId` = ?;";
        $this->sqlParams = [$linkId];

        return $this->doRowModification();
    }
}

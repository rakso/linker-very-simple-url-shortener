<?php

class SimpleDbClient
{
    protected  $useLegacyMethod;
    protected  $connection;
    protected  $sql = NULL;
    protected  $sqlParams = [];

    function __construct($servername, $username, $password, $dbname, $useLegacyMethod = false)
    {
        $this->connection = new mysqli($servername, $username, $password, $dbname);

        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        };

        // Set charset
        $this->connection->set_charset("utf8mb4");

        $this->useLegacyMethod = $useLegacyMethod;
    }

    function doQuery($getResult = false)
    {
        // return $this->connection->execute_query($this->sql, $this->sqlParams);

        $stmt = $this->connection->prepare($this->sql);

        $executeResult = null;

        if ($this->useLegacyMethod) {
            $paramsLength = count($this->sqlParams);
            if ($paramsLength) {
                $typeString = '';
                for ($i = 0; $i < $paramsLength; $i++)
                    $typeString .= 's';
                $stmt->bind_param($typeString, ...$this->sqlParams);
            }

            $executeResult = $stmt->execute();
        } else {
            $executeResult = $stmt->execute($this->sqlParams);
        }

        if ($executeResult) {
            if ($getResult) {
                return $stmt->get_result();
            }
            return $stmt;
        }
        return false;
    }

    function insert()
    {
        $result = $this->doQuery();

        return $result ?  $result->insert_id : $result;
    }

    function doRowModification()
    {
        $result = $this->doQuery();

        return $result ?  $result->affected_rows : $result;
    }

    function end()
    {
        $this->connection->close();
    }
}

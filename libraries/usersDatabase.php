<?php
require(__DIR__ .  '/../libraries/simpleDbClient.php');

class UsersDatabase extends SimpleDbClient
{
    function addUser($firstName, $lastName, $username, $password, $asActive = true)
    {
        $passwordSaltedHash = password_hash($password, PASSWORD_BCRYPT);
        $lastNameForSql = $lastName === null || $lastName === '' ? null : "$lastName";
        $this->sql = "INSERT INTO `users` (`pk`, `firstName`, `lastName`, `username`, `password`, `isActive`) VALUES (NULL, ?, ?, ?, ?, ?);";
        $this->sqlParams = [$firstName, $lastNameForSql, $username, $passwordSaltedHash, $asActive];

        return $this->insert();
    }

    function getUsers()
    {
        $this->sql = "SELECT `pk`, `firstName`, `lastName`, `username`, `isActive` FROM `users`;";

        $result = $this->doQuery(true);

        $data = [];

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($data, ['userId' => $row['pk'], 'firstName' => $row['firstName'], 'lastName' => $row['lastName'], 'username' => $row['username'], 'isActive' => $row['isActive']]);
            }
        }

        return $data;
    }

    function removeUser($userId)
    {
        $this->sql = "DELETE FROM `users` WHERE `users`.`pk` = ;";
        $this->sqlParams = [$userId];


        try {
            return $this->doRowModification();
        } catch (Exception $e) {
            if ($this->connection->errno == 1451) {
                throw new Exception("Can't remove user. There is some link data associated with this user.", 1);
            }
        }
    }

    function login($username, $password)
    {
        $this->sql = "SELECT `pk`,`password`,`isActive` FROM `users` WHERE `username` = ?;";
        $this->sqlParams = [$username];


        $result = $this->doQuery(true)->fetch_assoc();

        if ($result === null) {
            return false;
        }

        $passwordSaltedHash =  $result['password'];
        $userIsActive =  $result['isActive'];
        $userId =  $result['pk'];

        if (password_verify($password, $passwordSaltedHash)) {
            if ($userIsActive) {
                return $userId;
            }
        }

        return false;
    }

    function getUserFirstAndLastName($userId)
    {
        $this->sql = "SELECT `firstName`,`lastName` FROM `users` WHERE `pk` = ?;";
        $this->sqlParams = [$userId];


        $result = $this->doQuery(true)->fetch_assoc();

        if ($result === null) {
            return false;
        }

        $userFirstName =  $result['firstName'];
        $userlastName =  $result['lastName'];

        return [$userFirstName, $userlastName];
    }

    function getUsername($userId)
    {
        $this->sql = "SELECT `username` FROM `users` WHERE `pk` = ?;";
        $this->sqlParams = [$userId];

        $result = $this->doQuery(true)->fetch_assoc();

        if ($result === null) {
            return false;
        }

        $username =  $result['username'];
        return $username;
    }
}

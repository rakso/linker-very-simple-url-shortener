<?php
session_start();

class UserAuthorization
{
    private $sessionKey;

    function __construct()
    {
        $this->sessionKey = 'userLoggedInQr';
    }

    function setSessionAuthorization($userId)
    {
        $_SESSION[$this->sessionKey] = $userId;
    }

    function unsetSessionAuthorization()
    {
        unset($_SESSION[$this->sessionKey]);
    }

    function getCurrentAuthorizedUserId()
    {
        $authorizedUserId = isset($_SESSION[$this->sessionKey]) && $_SESSION[$this->sessionKey] !== '' ? $_SESSION[$this->sessionKey] : null;

        if ($authorizedUserId === null) {
            throw new Exception("Not authorized.", 1);
        }

        return $authorizedUserId;
    }

    function getCurrentAuthorizedUserIdOrDie()
    {
        try {
            return $this->getCurrentAuthorizedUserId();
        } catch (\Throwable $th) {
            die(json_encode(['error' => $th->getMessage()]));
        }
    }
}

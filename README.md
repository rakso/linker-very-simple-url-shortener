# Linker - very simple URL shortener

Simple URL shortener. Uses MySQL database and PHP 8.1.

## Getting started

- Create empty DB
- Set user and password
- Copy file `variables.template.php` and rename the copy to `variables.php`
- Copy file `variables.template.js` and rename the copy to `variables.js`
- Fill "variable" files with data.
- Run instalation: `https://<your-host-here>/manage/install.php`
- Done!
